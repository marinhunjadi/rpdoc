﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RpDoc
{
    class Program
    {
        public class Team
        {
            public string Name { get; set; }
            public int Points { get; set; }
            public int Goals { get; set; }
        }

        static void Main(string[] args)
        {
            string text = System.IO.File.ReadAllText(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())), "Results3.txt")).Replace("\r\n", "");
            string[] headers = { "Position", "Team", "Points", "Goal rate" };
            StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries;
            Char[] separator = { '"' };
            Hashtable teams = new Hashtable();
            string[] textArray = text.Split(separator, options);
            PrintRow(headers);
            foreach(string textItem in textArray)
            {
                string[] teamArray = textItem.Trim().Split(' ');

                if (teams.ContainsKey(teamArray[0]))
                {
                    Team team = (Team)teams[teamArray[0]];
                    team.Name = teamArray[0].ToUpper();
                    team.Points += Int32.Parse(teamArray[2]) > Int32.Parse(teamArray[3]) ? 2 : Int32.Parse(teamArray[2]) < Int32.Parse(teamArray[3]) ? 0 : 1;
                    team.Goals += Int32.Parse(teamArray[2]);
                    teams[team.Name] = team;
                }
                else
                {
                    Team team = new Team();
                    team.Name = teamArray[0].ToUpper();
                    team.Points = Int32.Parse(teamArray[2]) > Int32.Parse(teamArray[3]) ? 2 : Int32.Parse(teamArray[2]) < Int32.Parse(teamArray[3]) ? 0 : 1;
                    team.Goals = Int32.Parse(teamArray[2]);
                    teams[team.Name] = team;
                }
                if (teams.ContainsKey(teamArray[1]))
                {
                    Team team = (Team)teams[teamArray[1]];
                    team.Name = teamArray[1].ToUpper();
                    team.Points += Int32.Parse(teamArray[2]) < Int32.Parse(teamArray[3]) ? 2 : Int32.Parse(teamArray[2]) > Int32.Parse(teamArray[3]) ? 0 : 1;
                    team.Goals += Int32.Parse(teamArray[3]);
                    teams[team.Name] = team;
                }
                else
                {
                    Team team = new Team();
                    team.Name = teamArray[1].ToUpper();
                    team.Points = Int32.Parse(teamArray[2]) < Int32.Parse(teamArray[3]) ? 2 : Int32.Parse(teamArray[2]) > Int32.Parse(teamArray[3]) ? 0 : 1;
                    team.Goals = Int32.Parse(teamArray[3]);
                    teams[team.Name] = team;
                }
            }

            List<Team> values = teams.Values.OfType<Team>().ToList();
            IEnumerable<Team> results = values.AsEnumerable<Team>().OrderByDescending(t => t.Points).ThenByDescending(t => t.Goals);

            int i = 1;
            foreach (var result in results)
            {
                string[] r = { i.ToString(), result.Name, result.Points.ToString(), result.Goals.ToString() };
                PrintLine();
                PrintRow(r);
                i++;
            }
            PrintLine();

            using (XmlWriter writer = XmlWriter.Create(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())), "table.xml")))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("teams");
                int x = 1;
                foreach (var result in results)
                {
                    writer.WriteStartElement("team");

                    writer.WriteElementString("position", x.ToString());
                    writer.WriteElementString("name", result.Name);
                    writer.WriteElementString("points", result.Points.ToString());
                    writer.WriteElementString("goalrate", result.Goals.ToString());

                    writer.WriteEndElement();
                    x++;
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            Console.ReadLine();
        }

        static int tableWidth = 77;

        static void PrintLine()
        {
            Console.WriteLine(new string('-', tableWidth));
        }

        static void PrintRow(params string[] columns)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            string row = "|";

            foreach (string column in columns)
            {
                row += AlignCentre(column, width) + "|";
            }

            Console.WriteLine(row);
        }

        static string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }
    }
}
